﻿using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Games
{
    public class EditGame : PageModel
    {
        private readonly AppDatabaseContext _context;

        public EditGame(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public SaveGame SaveGame { get; set; } = default!;

        public ActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SaveGame = _context.SaveGames.First(s => s.SaveGameId == id);

            if (SaveGame == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || SaveGame == null)
            {
                return Page();
            }

            _context.Update(SaveGame);

            await _context.SaveChangesAsync();

            return RedirectToPage("./SavesList");
        }
    }
}