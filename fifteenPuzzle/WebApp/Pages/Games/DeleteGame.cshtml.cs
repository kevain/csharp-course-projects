﻿using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Games
{
    public class DeleteGame : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteGame(AppDatabaseContext context)
        {
            _context = context;
        }

        public SaveGame SaveGame { get; set; } = default!;

        public ActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SaveGame = _context.SaveGames.First(s => s.SaveGameId == id);

            if (SaveGame == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<ActionResult> OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SaveGame = _context.SaveGames.First(s => s.SaveGameId == id);

            if (SaveGame == null)
            {
                return NotFound();
            }

            _context.Remove(SaveGame);

            await _context.SaveChangesAsync();

            return RedirectToPage("./SavesList");
        }
    }
}