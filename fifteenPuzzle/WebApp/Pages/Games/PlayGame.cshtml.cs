﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using GameLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Games
{
    public class PlayGame : PageModel
    {
        private readonly AppDatabaseContext _context;

        public PlayGame(AppDatabaseContext context)
        {
            _context = context;
        }

        public Game Game { get; set; } = default!;

        public int saveId { get; set; }

        public ActionResult OnGet(int? id)
        {
            if (id == null)
            {
                return RedirectToPage("./SavesList");
            }

            var saveGame = _context.SaveGames.First(s => s.SaveGameId == id);

            if (saveGame == null)
            {
                return RedirectToPage("./SavesList");
            }

            Game = saveGame.CreateGame();
            saveId = saveGame.SaveGameId;

            return Page();
        }

        public async Task<ActionResult> OnPost(int? gameId, int? buttonId)
        {
            if (gameId == null || buttonId == null)
            {
                return RedirectToPage("./SavesList");
            }

            var saveGame = _context.SaveGames.First(s => s.SaveGameId == gameId);

            Game = saveGame.CreateGame();
            saveId = saveGame.SaveGameId;

            var gameButtonToMove = Game.GetGameButtonById((int) buttonId);

            if (Game.MoveGameButton(gameButtonToMove))
            {
                saveGame.UpdateSaveData(Game);

                _context.Update(saveGame);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./PlayGame", new {id = saveGame.SaveGameId});
        }
    }
}