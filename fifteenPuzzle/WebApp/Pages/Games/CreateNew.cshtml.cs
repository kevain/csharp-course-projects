﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using GameLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Games
{
    public class CreateNew : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateNew(AppDatabaseContext context)
        {
            _context = context;
        }

        public PageResult OnGet()
        {
            BoardSizeSelectList = new SelectList(GenerateValidSizesDict(), "Key", "Value");
            return Page();
        }

        public SelectList BoardSizeSelectList { get; set; } = default!;
        [BindProperty] public GameSettings GameSettings { get; set; } = default!;

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                BoardSizeSelectList = new SelectList(GenerateValidSizesDict(), "Key", "Value");
                return Page();
            }

            var game = new Game(GameSettings);
            game.InitGame();
            var save = new SaveGame(game);

            try
            {
                _context.SaveGames.Add(save);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("GameSettings.GameName",
                    "Game with such name already exists!\nPlease choose a new name or delete existing save!");
                BoardSizeSelectList = new SelectList(GenerateValidSizesDict(), "Key", "Value");
                return Page();
            }

            return RedirectToPage("./PlayGame", new {id = save.SaveGameId});
        }

        private static Dictionary<int, string> GenerateValidSizesDict()
        {
            var validSizes = new Dictionary<int, string>();

            for (var i = Gameboard.MinAxisLength; i <= Gameboard.MaxAxisLength; i++)
            {
                validSizes.Add(i, $"{i}x{i}");
            }

            return validSizes;
        }
    }
}