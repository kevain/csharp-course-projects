﻿using System.Collections.Generic;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Games
{
    public class SavesList : PageModel
    {
        private readonly AppDatabaseContext _context;

        public SavesList(AppDatabaseContext context)
        {
            _context = context;
        }
        
        public List<SaveGame> GameSaves { get; set; } = default!;

        public async void OnGet()
        {
            GameSaves = await _context.SaveGames.ToListAsync();
        }
    }
}