﻿using System;
using GameLogic;

namespace ConsoleAppLibrary
{
    public static class GameUi
    {
        private const char VerticalSeparator = '|';
        private const char HorizontalSeparator = '-';
        private const char CenterSeparator = '+';

        private const string BoardVerticalEdge = "||";
        private const char BoardHorizontalEdge = '=';

        private const int ButtonWidth = 6;

        public static void PrintGameStatistics(Game game)
        {
            var scoreString = $"Score: {game.Score}";
            var movesString = $"Moves: {game.Moves}";

            var lineWidth = game.Gameboard.BoardAxisLength * 6 - 1;

            Console.WriteLine("-".PadRight(lineWidth + 4, '-'));
            Console.WriteLine($"| {" ".PadRight(lineWidth)} |");
            Console.WriteLine($"| {scoreString.PadRight(lineWidth)} |");
            Console.WriteLine($"| {movesString.PadRight(lineWidth)} |");
            Console.WriteLine($"| {" ".PadRight(lineWidth)} |");
            Console.WriteLine("-".PadRight(lineWidth + 4, '-'));
        }

        public static void PrintGameBoard(ConsoleGame game)
        {
            var board = game.Gameboard.GameBoard;
            var boardSize = board.Length;
            var cursor = game.CurrentSelectionTileCoordinates;

            var boardHorizontalEdge = "".PadRight(boardSize * (ButtonWidth + 1) + 3, BoardHorizontalEdge);
            var buttonRowSeparator = GenerateButtonRowSeparator(boardSize);

            Console.WriteLine(boardHorizontalEdge);
            // rows
            for (var i = 0; i < boardSize; i++)
            {
                var boardRow = board[i];

                Console.Write(BoardVerticalEdge);
                // buttons
                for (var j = 0; j < boardSize; j++)
                {
                    var button = boardRow[j];
                    string buttonString;

                    if (button.Id == boardSize * boardSize)
                    {
                        if (game.IsFinished())
                        {
                            buttonString = $"{button}".PadLeft(ButtonWidth - 1).PadRight(ButtonWidth);
                        }
                        else
                        {
                            buttonString = GameButtonIsSelected(i, j, cursor)
                                ? "[  ]".PadLeft(ButtonWidth - 1).PadRight(ButtonWidth)
                                : "".PadRight(ButtonWidth);
                        }
                    }
                    else
                    {
                        buttonString = GameButtonIsSelected(i, j, cursor)
                            ? $"[{button.ToString().PadLeft(2)}]".PadLeft(ButtonWidth - 1).PadRight(ButtonWidth)
                            : $"{button}".PadLeft(ButtonWidth - 2).PadRight(ButtonWidth);
                    }

                    Console.Write(buttonString);

                    if (j == boardSize - 1) break;

                    Console.Write(VerticalSeparator);
                }

                Console.WriteLine(BoardVerticalEdge);

                if (i == boardSize - 1) break;

                Console.WriteLine(buttonRowSeparator);
            }

            Console.WriteLine(boardHorizontalEdge);
        }

        private static bool GameButtonIsSelected(int row, int col, GameButtonCoordinates cursorPosition)
        {
            return row == cursorPosition.Row && col == cursorPosition.Col;
        }

        private static string GenerateButtonRowSeparator(int boardSize)
        {
            var separator = BoardVerticalEdge;

            for (var i = 0; i < boardSize; i++)
            {
                separator += "".PadRight(ButtonWidth, HorizontalSeparator);
                if (i != boardSize - 1)
                {
                    separator += CenterSeparator;
                }
            }

            separator += BoardVerticalEdge;

            return separator;
        }
    }
}