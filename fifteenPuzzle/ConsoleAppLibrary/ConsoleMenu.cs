﻿using System;
using System.Collections.Generic;
using MenuSystem;

namespace ConsoleAppLibrary
{
    public class ConsoleMenu: Menu
    {
        public const string CloseMenu = "ConsoleMenu:CloseMenu";

        private const int MoveSelectionUp = -1;
        private const int MoveSelectionDown = 1;
        
        private int _selectedMenuItemIndex;

        public int SelectedMenuItemIndex
        {
            get => _selectedMenuItemIndex;
            private set
            {
                switch (value)
                {
                    case (MoveSelectionDown):
                        if (_selectedMenuItemIndex < MenuItems.Count - 1)
                        {
                            _selectedMenuItemIndex++;
                        } 
                        
                        break;
                    
                    case (MoveSelectionUp):
                        if (_selectedMenuItemIndex > 0)
                        {
                            _selectedMenuItemIndex--;
                        }
                        break;
                    
                    default:
                        break;
                }
            }
        }

        public ConsoleMenu(string title, List<MenuItem> menuItems) : base(title, menuItems)
        {
        }

        public string Open()
        {
            var cmd = "";

            do
            {
                MenuUi.PrintMenu(this);
                
                var key = Console.ReadKey().Key;

                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        SelectedMenuItemIndex = MoveSelectionUp;
                        break;
                    
                    case ConsoleKey.DownArrow:
                        SelectedMenuItemIndex = MoveSelectionDown;
                        break;
                    
                    case ConsoleKey.Enter:
                        var menuItem = MenuItems[SelectedMenuItemIndex];

                        if (menuItem.FuncToExecute != null)
                        {
                            cmd = menuItem.FuncToExecute();
                        }
                        break;
                    
                    case ConsoleKey.Backspace:
                        cmd = CloseMenu;
                        break;
                    
                    default:
                        break;
                }
                
            } while (cmd != CloseMenu);
            
            return "";
        }
    }
}