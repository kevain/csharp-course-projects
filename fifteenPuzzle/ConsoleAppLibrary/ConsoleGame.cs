﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL;
using Domain;
using GameLogic;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace ConsoleAppLibrary
{
    public class ConsoleGame : Game
    {
        private const string MoveCursorLeft = "ConsoleGame:MoveCursorLeft";
        private const string MoveCursorRight = "ConsoleGame:MoveCursorRight";
        private const string MoveCursorUp = "ConsoleGame:MoveCursorUp";
        private const string MoveCursorDown = "ConsoleGame:MoveCursorDown";

        public GameButtonCoordinates CurrentSelectionTileCoordinates { get; set; } =
            new GameButtonCoordinates(0, 0);

        public ConsoleGame(GameSettings gameSettings) : base(gameSettings)
        {
            InitGame();
        }

        public ConsoleGame(Game game)
        {
            Title = game.Title;
            Score = game.Score;
            Gameboard = game.Gameboard;
            Moves = game.Moves;
        }

        public string Run()
        {
            ConsoleKey key;

            do
            {
                Console.Clear();
                GameUi.PrintGameStatistics(this);
                GameUi.PrintGameBoard(this);

                Console.WriteLine("Use arrow keys to move cursor.\n" +
                                  "Press enter to move selected button.\n" +
                                  "Press S to save game.\n" +
                                  "Press R to reset.\n" +
                                  "Press Q to quit.\n");

                key = Console.ReadKey().Key;

                switch (key)
                {
                    case ConsoleKey.R:
                        Console.WriteLine("----------");
                        Console.WriteLine("Are you sure you want to reset? Press enter to confirm.");

                        key = Console.ReadKey().Key;

                        if (key == ConsoleKey.Enter)
                        {
                            ResetGame();
                        }

                        break;

                    case ConsoleKey.S:
                        Console.WriteLine("----------");

                        Console.WriteLine("Saving...");

                        try
                        {
                            using (var db = new AppDatabaseContext())
                            {
                                var saveGame = new SaveGame(this);
                                db.SaveGames.Add(saveGame);
                                db.SaveChanges();
                            }
                        }
                        catch (DbUpdateException)
                        {
                            Console.WriteLine(
                                "Do you wish to overwrite existing save? Press enter to confirm or any other key to cancel.");
                            key = Console.ReadKey().Key;

                            if (key != ConsoleKey.Enter)
                            {
                                break;
                            }

                            Console.WriteLine("Overwriting savefile...");

                            using (var db = new AppDatabaseContext())
                            {
                                var save = db.SaveGames.First(s => s.SaveName == Title);
                                save.UpdateSaveData(this);
                                db.Update(save);

                                db.SaveChanges();
                            }
                        }

                        Console.WriteLine("Game saved!");
                        Console.WriteLine("Press any key to return to game...");
                        Console.ReadKey();

                        break;

                    case ConsoleKey.Q:
                        Console.WriteLine("Press enter to quit without saving or any other key to return to game.");
                        key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Enter)
                        {
                            return "quit";
                        }

                        break;

                    case ConsoleKey.UpArrow:
                        MoveGameBoardCursor(MoveCursorUp);
                        break;

                    case ConsoleKey.DownArrow:
                        MoveGameBoardCursor(MoveCursorDown);
                        break;

                    case ConsoleKey.LeftArrow:
                        MoveGameBoardCursor(MoveCursorLeft);
                        break;

                    case ConsoleKey.RightArrow:
                        MoveGameBoardCursor(MoveCursorRight);
                        break;

                    case ConsoleKey.Enter:
                        MoveSelectedButton();
                        break;
                }
            } while (!IsFinished());

            Console.Clear();
            GameUi.PrintGameStatistics(this);
            GameUi.PrintGameBoard(this);

            Console.WriteLine("----------");
            Console.WriteLine("Game over!");
            Console.WriteLine("Press any key to return to menu!");
            Console.ReadKey();

            return "quit";
        }

        private void MoveSelectedButton()
        {
            var coords = CurrentSelectionTileCoordinates;
            var gameButton = Gameboard.GameBoard[coords.Row][coords.Col];

            MoveGameButton(gameButton);
        }

        private void MoveGameBoardCursor(string direction)
        {
            switch (direction)
            {
                case MoveCursorLeft:
                    if (CurrentSelectionTileCoordinates.Col > 0)
                    {
                        CurrentSelectionTileCoordinates.Col -= 1;
                    }

                    break;

                case MoveCursorRight:
                    if (CurrentSelectionTileCoordinates.Col < Gameboard.BoardAxisLength - 1)
                    {
                        CurrentSelectionTileCoordinates.Col += 1;
                    }

                    break;

                case MoveCursorUp:
                    if (CurrentSelectionTileCoordinates.Row > 0)
                    {
                        CurrentSelectionTileCoordinates.Row -= 1;
                    }

                    break;

                case MoveCursorDown:
                    if (CurrentSelectionTileCoordinates.Row < Gameboard.BoardAxisLength - 1)
                    {
                        CurrentSelectionTileCoordinates.Row += 1;
                    }

                    break;
            }
        }
    }
}