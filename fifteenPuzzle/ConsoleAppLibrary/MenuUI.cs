﻿using System;

namespace ConsoleAppLibrary
{
    public static class MenuUi
    {
        public static void PrintMenu(ConsoleMenu consoleMenu)
        {
            Console.Clear();
            Console.WriteLine($"====== {consoleMenu.Title} ======");

            for (var i = 0; i < consoleMenu.MenuItems.Count; i++)
            {
                var menuItem = consoleMenu.MenuItems[i];

                Console.WriteLine(i == consoleMenu.SelectedMenuItemIndex ? $"> {menuItem}" : $"  {menuItem}");
            }
        }
    }
}