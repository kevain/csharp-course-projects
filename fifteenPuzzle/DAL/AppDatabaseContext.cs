﻿using System;
using System.IO;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDatabaseContext : DbContext
    {
        public DbSet<SaveGame> SaveGames { get; set; }

        public static readonly string DbPath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "FifteenPuzzleGameDb.db");

        public AppDatabaseContext()
        {
        }

        public AppDatabaseContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlite("Data Source=" + DbPath);

//            optionsBuilder.UseLoggerFactory(MyLoggerFactory); // Logging
        }

        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => { builder.AddConsole(); });

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<SaveGame>()
                .HasIndex(s => s.SaveName)
                .IsUnique();
        }
    }
}