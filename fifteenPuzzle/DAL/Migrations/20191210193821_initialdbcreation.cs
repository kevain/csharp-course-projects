﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class initialdbcreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SaveGames",
                columns: table => new
                {
                    SaveGameId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SaveName = table.Column<string>(nullable: true),
                    SaveDateTime = table.Column<DateTime>(nullable: false),
                    GameScore = table.Column<int>(nullable: false),
                    GameMoves = table.Column<int>(nullable: false),
                    GameBoardAxisLength = table.Column<int>(nullable: false),
                    GameBoardStateJson = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaveGames", x => x.SaveGameId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SaveGames_SaveName",
                table: "SaveGames",
                column: "SaveName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SaveGames");
        }
    }
}
