﻿using System.Collections.Generic;

namespace MenuSystem
{
    public class Menu
    {
        public string Title { get; set; }
        public List<MenuItem> MenuItems { get; set; }

        public Menu(string title, List<MenuItem> menuItems)
        {
            Title = title;
            MenuItems = menuItems;
        }
    }
}