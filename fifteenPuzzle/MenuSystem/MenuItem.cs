﻿using System;

namespace MenuSystem
{
    public class MenuItem
    {
        public const int TitleMinLength = 3;
        public const int TitleMaxLength = 15;

        private string _title = default!;

        public string Title
        {
            get => _title;
            set
            {
                value = value.Trim();

                if (value.Length < TitleMinLength || value.Length > TitleMaxLength)
                {
                    throw new ArgumentException(
                        $"Menu item length must be between {TitleMinLength} and {TitleMaxLength}");
                }

                _title = value;
            }
        }

        public Func<string> FuncToExecute { get; set; }

        public MenuItem(string title, Func<string> funcToExecute)
        {
            Title = title;
            FuncToExecute = funcToExecute;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}