﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using GameLogic;

namespace Domain
{
    public class SaveGame
    {
        public int SaveGameId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(15)]
        [Display(Name = "Save's name")]
        public string SaveName { get; set; } = default!;

        [Display(Name = "Saved at")] [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy @ HH:mm}")] public DateTime SaveDateTime { get; set; }
        [Display(Name = "Score")] public int GameScore { get; set; }
        [Display(Name = "Moves")] public int GameMoves { get; set; }

        [Display(Name = "Board Size")] public int GameBoardAxisLength { get; set; }
        public string GameBoardStateJson { get; set; } = default!;

        public SaveGame()
        {
            // needed for db
        }

        public SaveGame(Game game)
        {
            SaveName = game.Title;
            GameScore = game.Score;
            GameMoves = game.Moves;
            GameBoardAxisLength = game.Gameboard.BoardAxisLength;
            GameBoardStateJson = JsonSerializer.Serialize(game.Gameboard);
            SaveDateTime = DateTime.Now;
        }

        public void UpdateSaveData(Game game)
        {
            GameScore = game.Score;
            GameMoves = game.Moves;
            GameBoardStateJson = JsonSerializer.Serialize(game.Gameboard);
            SaveDateTime = DateTime.Now;
        }

        public Game CreateGame()
        {
            return new Game
            {
                Title = SaveName,
                Moves = GameMoves,
                Score = GameScore,
                Gameboard = JsonSerializer.Deserialize<Gameboard>(GameBoardStateJson)
            };
        }
    }
}