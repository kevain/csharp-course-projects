# FifteenPuzzle Game

Developed as the course project for C# course.

The solution has two runnable apps - game can be played in console and through web browser. Both apps (console app and web app) use the same game logic and same database, so the user can start the game on one app, save and continue in the other app. 

Rules etc. on [Wikipedia](https://en.wikipedia.org/wiki/15_puzzle).

## Screenshots
### Game in console

![Screenshot - game in console](./screenshots/console-game.jpg)

### Game in web browser
![Screenshot - game in web broswer](./screenshots/browser-game.jpg)
