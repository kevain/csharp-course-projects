﻿namespace GameLogic
{
    public class GameButtonCoordinates
    {
        public int Row { get; set; }
        public int Col { get; set; }

        public GameButtonCoordinates(int row, int col)
        {
            Row = row;
            Col = col;
        }
    }
}