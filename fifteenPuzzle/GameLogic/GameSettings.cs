﻿using System.ComponentModel.DataAnnotations;

namespace GameLogic
{
    public class GameSettings
    {
        public GameSettings(string name, int boardSize)
        {
            GameName = name;
            BoardAxisLength = boardSize;
        }

        public GameSettings()
        {
            // needed for webapp
        }

        [Required]
        [MinLength(3)]
        [MaxLength(15)]
        [Display(Name = "Game's Name")]
        public string GameName { get; set; } = default!;

        [Required]
        [Range(Gameboard.MinAxisLength, Gameboard.MaxAxisLength)]
        [Display(Name = "Board Size")]
        public int BoardAxisLength { get; set; }
    }
}