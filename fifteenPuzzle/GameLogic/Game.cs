﻿namespace GameLogic
{
    public class Game
    {
        public const int TitleMinLength = 3;
        public const int TitleMaxLength = 15;
        public string Title { get; set; } = "Puzzle 15";
        public Gameboard Gameboard { get; set; }
        public int Score { get; set; }
        public int Moves { get; set; }

        public Game(GameSettings gameSettings)
        {
            Title = gameSettings.GameName;
            Gameboard = new Gameboard(gameSettings.BoardAxisLength);
            Score = 0;
            Moves = 0;
        }

        public Game()
        {
            Gameboard = new Gameboard();
            Score = 0;
            Moves = 0;
        }

        public void InitGame()
        {
            Gameboard.InitGameBoard();
            Gameboard.ShuffleGameBoard();
        }

        public void ResetGame()
        {
            Moves = 0;
            Score = 0;
            Gameboard.ShuffleGameBoard();
        }

        public bool MoveGameButton(GameButton gameButton)
        {
            if (!Gameboard.GameButtonCanBeMoved(gameButton)) return false;

            Gameboard.MoveGameButton(gameButton);
            Moves += 1;

            UpdateScore();

            return true;
        }

        public GameButton GetGameButtonById(int id)
        {
            return Gameboard.GetGameButtonById(id);
        }

        private void UpdateScore()
        {
            if (Moves < 100)
            {
                Score += 5;
            }
            else
            {
                Score -= 5;
            }
        }

        public bool IsFinished()
        {
            var bal = Gameboard.BoardAxisLength - 1;

            if (Gameboard.GameBoard[bal][bal].Id != Gameboard.BoardAxisLength * Gameboard.BoardAxisLength)
            {
                return false;
            }

            var expectedId = 1;

            foreach (var row in Gameboard.GameBoard)
            {
                foreach (var gameButton in row)
                {
                    if (gameButton.Id != expectedId)
                    {
                        return false;
                    }

                    expectedId++;
                }
            }

            return true;
        }
    }
}