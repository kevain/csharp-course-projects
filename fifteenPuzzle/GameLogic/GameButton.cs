﻿namespace GameLogic
{
    public class GameButton
    {
        public int Id { get; set; }

        public GameButton()
        {
            // This empty constructor is needed for loading game from json.
        }

        public GameButton(int id)
        {
            Id = id;
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is GameButton gameButton && (Id == gameButton.Id);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }
    }
}