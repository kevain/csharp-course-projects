﻿using System;
using System.Linq;

namespace GameLogic
{
    public class Gameboard
    {
        public const int MinAxisLength = 3;
        public const int MaxAxisLength = 5;
        public int BoardAxisLength { get; set; }

        public GameButton[][] GameBoard { get; set; } = default!;

        public Gameboard()
        {
            // This empty constructor is needed for loading game from json.
        }

        public Gameboard(int boardAxisLength = 4)
        {
            if (boardAxisLength < MinAxisLength || boardAxisLength > MaxAxisLength)
            {
                throw new ArgumentOutOfRangeException($"{boardAxisLength} is not a valid axisLength. " +
                                                      $"Must be between {MinAxisLength} ... {MaxAxisLength}");
            }

            BoardAxisLength = boardAxisLength;
        }

        public void InitGameBoard()
        {
            GameBoard = new GameButton[BoardAxisLength][];

            for (var row = 0; row < BoardAxisLength; row++)
            {
                GameBoard[row] = new GameButton[BoardAxisLength];

                for (var col = 0; col < BoardAxisLength; col++)
                {
                    var id = row * BoardAxisLength + (col + 1);
                    var gameButton = new GameButton(id);
                    GameBoard[row][col] = gameButton;
                }
            }
        }

        public void ShuffleGameBoard()
        {
            for (var i = 0; i < 1500; i++)
            {
                var random = new Random();
                var row = random.Next(0, BoardAxisLength);
                var col = random.Next(0, BoardAxisLength);

                var gameButton = GameBoard[row][col];

                if (GameButtonCanBeMoved(gameButton))
                {
                    MoveGameButton(gameButton);
                }
            }
        }

        public void MoveGameButton(GameButton gameButton)
        {
            // find "coordinates" for gameButton
            var buttonToMoveCoords = GetGameButtonCoordinates(gameButton);
            // find "coordinates for the "invisible" gameButton
            var invisibleButtonId = BoardAxisLength * BoardAxisLength;
            var invisibleButtonCoords =
                GetGameButtonCoordinates(
                    new GameButton(invisibleButtonId)); // Maybe should find the existing one instead?

            GameBoard[buttonToMoveCoords.Row][buttonToMoveCoords.Col] = new GameButton(invisibleButtonId);
            GameBoard[invisibleButtonCoords.Row][invisibleButtonCoords.Col] = gameButton;
        }

        public bool GameButtonCanBeMoved(GameButton gameButton)
        {
            // find "coordinates" for gameButton
            var buttonToMoveCoords = GetGameButtonCoordinates(gameButton);
            // find "coordinates for the "invisible" gameButton
            var invisibleButtonId = BoardAxisLength * BoardAxisLength;
            var invisibleButtonCoords =
                GetGameButtonCoordinates(
                    new GameButton(invisibleButtonId)); // Maybe should find the existing one instead?

            // compare if they are on same axis
            if (buttonToMoveCoords.Row != invisibleButtonCoords.Row &&
                buttonToMoveCoords.Col != invisibleButtonCoords.Col)
            {
                return false;
            }

            // check for empty space above/below
            if (buttonToMoveCoords.Col == invisibleButtonCoords.Col)
            {
                return buttonToMoveCoords.Row - 1 == invisibleButtonCoords.Row ||
                       buttonToMoveCoords.Row + 1 == invisibleButtonCoords.Row;
            }

            // check for empty space left/right
            if (buttonToMoveCoords.Row == invisibleButtonCoords.Row)
            {
                return buttonToMoveCoords.Col - 1 == invisibleButtonCoords.Col ||
                       buttonToMoveCoords.Col + 1 == invisibleButtonCoords.Col;
            }

            return false;
        }

        public GameButton GetGameButtonById(int id)
        {
            if (id < 1 || id > BoardAxisLength * BoardAxisLength)
            {
                throw new ArgumentOutOfRangeException($"{id} is not a valid GameButton");
            }

            return GameBoard.SelectMany(row => row).FirstOrDefault(gameButton => gameButton.Id == id);
        }

        private GameButtonCoordinates GetGameButtonCoordinates(GameButton gameButton)
        {
            for (var row = 0; row < BoardAxisLength; row++)
            {
                for (var col = 0; col < BoardAxisLength; col++)
                {
                    var element = GameBoard[row][col];

                    if (gameButton.Equals(element))
                    {
                        return new GameButtonCoordinates(row, col);
                    }
                }
            }

            throw new Exception($"GameButton {gameButton} could not be found on board.");
        }

        public override string ToString()
        {
            var str = "";

            str += new string('-', BoardAxisLength * 5);
            str += "\n";

            foreach (var row in GameBoard)
            {
                str += "||";
                foreach (var gameButton in row)
                {
                    if (gameButton.Id == BoardAxisLength * BoardAxisLength)
                    {
                        str += $"    |";
                    }
                    else
                    {
                        str += $"  {gameButton}  |";
                    }
                }

                str += "|\n";
            }


            str += new string('-', BoardAxisLength * 5);

            return str;
        }
    }
}