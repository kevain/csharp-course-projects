﻿using System;
using System.Collections.Generic;
using ConsoleAppLibrary;
using DAL;
using Domain;
using GameLogic;
using MenuSystem;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainMenu = new ConsoleMenu("Puzzle 15", new List<MenuItem>
            {
                new MenuItem("New Game", () => SetUpNewGame()),
                new MenuItem("Load Game", () => LoadGameMenu()),
                new MenuItem("Go back", () => ConsoleMenu.CloseMenu)
            });

            var titleScreenMenu = new ConsoleMenu("Puzzle 15", new List<MenuItem>
            {
                new MenuItem("Start", () => mainMenu.Open()),
                new MenuItem("QUIT", () => ConsoleMenu.CloseMenu)
            });

            titleScreenMenu.Open();
        }

        private static string LoadGameMenu()
        {
            var loadGameMenu = new ConsoleMenu("Load Game", new List<MenuItem>());
            var menuItems = loadGameMenu.MenuItems;

            using (var dbContext = new AppDatabaseContext())
            {
                foreach (var gameSave in dbContext.SaveGames)
                {
                    try
                    {
                        menuItems.Add(new MenuItem(gameSave.SaveName, () => RunGame(gameSave)));
                    }
                    catch (Exception)
                    {
                        // Avoid crashing if someone has altered database
                    }
                }
            }


            menuItems.Add(new MenuItem("Go back", () => ConsoleMenu.CloseMenu));
            loadGameMenu.Open();

            return "";
        }

        private static string SetUpNewGame()
        {
            string name;
            do
            {
                Console.Clear();
                Console.WriteLine("Choose a name for your game: " +
                                  $"(Length must be in range {Game.TitleMinLength}...{Game.TitleMaxLength})");
                Console.Write("> ");

                name = Console.ReadLine()?.Trim() ?? "";
            } while (name.Length < Game.TitleMinLength || name.Length > Game.TitleMaxLength);

            int size;
            do
            {
                Console.Clear();
                Console.WriteLine("What size should the board be? " +
                                  $"Must be in range {Gameboard.MinAxisLength}...{Gameboard.MaxAxisLength}.");
                Console.Write("> ");
                int.TryParse(Console.ReadLine(), out size);
            } while (size < Gameboard.MinAxisLength || size > Gameboard.MaxAxisLength);

            new ConsoleGame(new GameSettings(name, size)).Run();

            return "";
        }

        private static string RunGame(SaveGame saveGame)
        {
            var game = saveGame.CreateGame();
            return new ConsoleGame(game).Run();
        }
    }
}