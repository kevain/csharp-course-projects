using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Books
{
    public class EditModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public EditModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Book Book { get; set; } = default!;

        private Author Author { get; set; } = default!;

        private List<Category> Categories { get; set; } = default!;
        private List<Language> Languages { get; set; } = default!;
        private List<Publisher> Publishers { get; set; } = default!;
        private List<Author> Authors { get; set; } = default!;

        public SelectList CategorySelectList { get; set; } = default!;
        public SelectList LanguageSelectList { get; set; } = default!;
        public SelectList PublisherSelectList { get; set; } = default!;
        public SelectList AuthorSelectList { get; set; } = default!;

        private int SelectedCategoryId { get; set; }
        private int SelectedLanguageId { get; set; }
        private int SelectedPublisherId { get; set; }

        [BindProperty]
        [Display(Name = "Author")]
        public int SelectedAuthorId { get; set; } // binded because it can't be directly glued to Book

        public async Task<IActionResult> OnGetAsync(int? bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }

            Book = await _context.Books
                .Include(b => b.Category)
                .Include(b => b.Language)
                .Include(b => b.Publisher)
                .Include(book => book.BookAuthors)
                .ThenInclude(ba => ba.Author)
                .FirstOrDefaultAsync(m => m.BookId == bookId);

            if (Book == null)
            {
                return NotFound();
            }

            await GetCategoryLanguagePublisher();
            SelectedAuthorId = Book.BookAuthors.First().AuthorId;
            PopulateSelectLists();

            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Author = await _context.Authors.FirstOrDefaultAsync(author => author.AuthorId == SelectedAuthorId);

            if (!ModelState.IsValid || Author == null)
            {
                await GetCategoryLanguagePublisher();
                PopulateSelectLists();
                return Page();
            }

            // Remove existing author-book join
            Book.BookAuthors = await _context.AuthorBooks.Where(ab => ab.BookId == Book.BookId).ToListAsync();
            _context.Attach(Book);
            Book.BookAuthors = null;
            await _context.SaveChangesAsync();

            Book.BookAuthors = new List<AuthorBook>()
            {
                new AuthorBook()
                {
                    Author = Author,
                    Book = Book
                }
            };

            _context.Update(Book);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(Book.BookId))
                {
                    return NotFound();
                }

                throw;
            }

            return RedirectToPage("./Details", new {bookId = Book.BookId});
            ;
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.BookId == id);
        }

        private async Task GetCategoryLanguagePublisher()
        {
            Categories = await _context.Categories.OrderBy(category => category.CategoryName).ToListAsync();
            Languages = await _context.Languages.OrderBy(language => language.LanguageName).ToListAsync();
            Publishers = await _context.Publishers.OrderBy(publisher => publisher.PublisherName).ToListAsync();
            Authors = await _context.Authors.OrderBy(author => author.FirstName)
                .ThenBy(author => author.LastName)
                .ToListAsync();
        }

        private void PopulateSelectLists()
        {
            CategorySelectList = new SelectList(Categories,
                nameof(Category.CategoryId),
                nameof(Category.CategoryName),
                SelectedCategoryId);

            PublisherSelectList = new SelectList(Publishers,
                nameof(Publisher.PublisherId),
                nameof(Publisher.PublisherName),
                SelectedPublisherId);

            LanguageSelectList = new SelectList(Languages,
                nameof(Language.LanguageId),
                nameof(Language.LanguageName),
                SelectedLanguageId);

            AuthorSelectList = new SelectList(Authors,
                nameof(Domain.Author.AuthorId),
                nameof(Domain.Author.FirstLastName),
                SelectedAuthorId);
        }
    }
}