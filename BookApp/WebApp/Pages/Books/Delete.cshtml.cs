using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace WebApp.Pages.Books
{
    public class DeleteModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Book Book { get; set; } = default!;
        public Language Language => Book.Language!;
        public Publisher Publisher => Book.Publisher!;
        public Category Category => Book.Category!;

        public async Task<IActionResult> OnGetAsync(int? bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }

            Book = await _context.Books
                .Include(b => b.Category)
                .Include(b => b.Language)
                .Include(b => b.Publisher)
                .Include(book => book.BookAuthors)
                .ThenInclude(bookAuthors => bookAuthors.Author)
                .FirstOrDefaultAsync(m => m.BookId == bookId);

            if (Book == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }

            Book = await _context.Books.FindAsync(bookId);

            if (Book != null)
            {
                _context.Books.Remove(Book);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}