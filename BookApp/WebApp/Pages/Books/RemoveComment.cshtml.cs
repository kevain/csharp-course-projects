using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Books
{
    public class RemoveCommentModel : PageModel
    {
        private readonly DAL.AppDatabaseContext _context;

        public RemoveCommentModel(DAL.AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Comment Comment { get; set; } = default!;

        public Book Book => Comment.Book!;

        public async Task<IActionResult> OnGetAsync(int? commentId)
        {
            if (commentId == null)
            {
                return NotFound();
            }

            Comment = await _context.Comments
                .Include(c => c.Book).FirstOrDefaultAsync(m => m.CommentId == commentId);

            if (Comment == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? commentId)
        {
            if (commentId == null)
            {
                return NotFound();
            }

            Comment = await _context.Comments.FindAsync(commentId);

            if (Comment == null) return NotFound();
            
            _context.Comments.Remove(Comment);
            await _context.SaveChangesAsync();
            return RedirectToPage("./Details", new {bookid = Comment.BookId});
        }
    }
}