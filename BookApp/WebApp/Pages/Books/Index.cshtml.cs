using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Books
{
    public class IndexModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public IndexModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IList<Book> Books { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            IQueryable<Book> booksQuery = _context.Books
                .Include(book => book.Comments)
                .Include(book => book.Category)
                .Include(book => book.BookAuthors)
                .ThenInclude(bookAuthor => bookAuthor.Author);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();
                booksQuery = booksQuery.Where(book => book.Title.ToLower().Contains(Search));
            }

            Books = await booksQuery
                .OrderBy(book => book.Title)
                .ThenByDescending(book => book.YearPublished)
                .ToListAsync();

            if (Books.Count == 1 && search != null)
            {
                return RedirectToPage("./Details", new {bookId = Books.First().BookId});
            }

            return Page();
        }
    }
}