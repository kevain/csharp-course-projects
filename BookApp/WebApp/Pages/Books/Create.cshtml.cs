using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Books
{
    public class CreateModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGet(int? languageId, int? authorId, int? publisherId, int? categoryId)
        {
            if (languageId != null)
            {
                SelectedLanguageId = (int) languageId;
            }

            if (publisherId != null)
            {
                SelectedPublisherId = (int) publisherId;
            }

            if (categoryId != null)
            {
                SelectedCategoryId = (int) categoryId;
            }

            if (authorId != null)
            {
                SelectedAuthorId = (int) authorId;
            }

            await GetCategoryLanguagePublisher();
            PopulateSelectLists();

            return Page();
        }

        [BindProperty] public Book Book { get; set; } = default!;
        private Author Author { get; set; } = default!;

        public List<Category> Categories { get; set; } = default!;
        public List<Language> Languages { get; set; } = default!;
        public List<Publisher> Publishers { get; set; } = default!;
        public List<Author> Authors { get; set; } = default!;

        public SelectList CategorySelectList { get; set; } = default!;
        public SelectList LanguageSelectList { get; set; } = default!;
        public SelectList PublisherSelectList { get; set; } = default!;
        public SelectList AuthorSelectList { get; set; } = default!;

        private int SelectedCategoryId { get; set; }
        private int SelectedLanguageId { get; set; }
        private int SelectedPublisherId { get; set; }

        [BindProperty]
        [Display(Name = "Author")]
        public int SelectedAuthorId { get; set; } // binded because it can't be directly glued to Book

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Author = await _context.Authors.FirstOrDefaultAsync(author => author.AuthorId.Equals(SelectedAuthorId));

            if (Author == null || Book == null)
            {
                return Page();
            }

            Book.BookAuthors = new List<AuthorBook>()
            {
                new AuthorBook()
                {
                    Author = Author,
                    Book = Book
                }
            };

            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            _context.Books.Add(Book);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {bookId = Book.BookId});
        }

        private async Task GetCategoryLanguagePublisher()
        {
            Categories = await _context.Categories.OrderBy(category => category.CategoryName).ToListAsync();
            Languages = await _context.Languages.OrderBy(language => language.LanguageName).ToListAsync();
            Publishers = await _context.Publishers.OrderBy(publisher => publisher.PublisherName).ToListAsync();
            Authors = await _context.Authors.OrderBy(author => author.FirstName)
                .ThenBy(author => author.LastName)
                .ToListAsync();
        }

        private void PopulateSelectLists()
        {
            CategorySelectList = new SelectList(Categories,
                nameof(Category.CategoryId),
                nameof(Category.CategoryName),
                SelectedCategoryId);

            PublisherSelectList = new SelectList(Publishers,
                nameof(Publisher.PublisherId),
                nameof(Publisher.PublisherName),
                SelectedPublisherId);

            LanguageSelectList = new SelectList(Languages,
                nameof(Language.LanguageId),
                nameof(Language.LanguageName),
                SelectedLanguageId);

            AuthorSelectList = new SelectList(Authors,
                nameof(Domain.Author.AuthorId),
                nameof(Domain.Author.FirstLastName),
                SelectedAuthorId);
        }
    }
}