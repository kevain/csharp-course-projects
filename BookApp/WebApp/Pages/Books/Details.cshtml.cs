using System;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Books
{
    public class DetailsModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DetailsModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public Book Book { get; set; } = default!;
        public Language Language => Book.Language!;
        public Publisher Publisher => Book.Publisher!;
        public Category Category => Book.Category!;
        [BindProperty] public Comment Comment { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }

            Book = await GetBookFromDb(bookId);

            if (Book == null)
            {
                return NotFound();
            }

            Comment = new Comment() {BookId = Book.BookId};

            return Page();
        }

        private async Task<Book> GetBookFromDb(int? bookId)
        {
            return await _context.Books
                .Include(b => b.Category)
                .Include(b => b.Language)
                .Include(b => b.Publisher)
                .Include(b => b.Comments)
                .Include(book => book.BookAuthors)
                .ThenInclude(ab => ab.Author)
                .FirstOrDefaultAsync(m => m.BookId == bookId);
        }

        public async Task<IActionResult> OnPostAsync(int? bookId)
        {
            if (Comment == null) return Page();
            
            if (!ModelState.IsValid)
            {
                Book = await GetBookFromDb(Comment.BookId);
                return Page();
            }


            _context.Comments.Add(Comment);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {bookId = Comment.BookId});
        }
    }
}