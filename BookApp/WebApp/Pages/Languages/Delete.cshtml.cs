using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Languages
{
    public class DeleteModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Language Language { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? languageId)
        {
            if (languageId == null)
            {
                return NotFound();
            }

            Language = await _context.Languages.FirstOrDefaultAsync(m => m.LanguageId == languageId);

            if (Language == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? languageId)
        {
            if (languageId == null)
            {
                return NotFound();
            }

            Language = await _context.Languages.FindAsync(languageId);

            if (Language != null)
            {
                _context.Languages.Remove(Language);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}