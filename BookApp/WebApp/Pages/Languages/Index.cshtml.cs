using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Languages
{
    public class IndexModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public IndexModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IList<Language> Languages { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            IQueryable<Language> languageQuery = _context.Languages
                .Include(language => language.Books);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();
                languageQuery = languageQuery.Where(language => language.LanguageName.ToLower().Contains(Search));
            }

            Languages = await languageQuery
                .OrderBy(language => language.LanguageName)
                .ToListAsync();

            if (Languages.Count == 1 && search != null)
            {
                return RedirectToPage("./Details", new {languageId = Languages.First().LanguageId});
            }

            return Page();
        }
    }
}