using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Languages
{
    public class EditModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public EditModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Language Language { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? languageId)
        {
            if (languageId == null)
            {
                return NotFound();
            }

            Language = await _context.Languages.FirstOrDefaultAsync(m => m.LanguageId == languageId);

            if (Language == null)
            {
                return NotFound();
            }

            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Language).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LanguageExists(Language.LanguageId))
                {
                    return NotFound();
                }

                throw;
            }

            return RedirectToPage("./Details", new {languageId = Language.LanguageId});
        }

        private bool LanguageExists(int id)
        {
            return _context.Languages.Any(e => e.LanguageId == id);
        }
    }
}