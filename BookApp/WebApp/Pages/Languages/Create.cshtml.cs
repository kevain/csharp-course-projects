using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Languages
{
    public class CreateModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Language Language { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || Language == null)
            {
                return Page();
            }

            var existingLanguage = await _context.Languages.FirstOrDefaultAsync(language =>
                language.LanguageName.ToLower().Equals(Language.LanguageName.ToLower()));

            if (existingLanguage != null)
            {
                return RedirectToPage("./Details", new {languageId = existingLanguage.LanguageId});
            }
            
            _context.Languages.Add(Language);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {languageId = Language.LanguageId});
        }
    }
}