using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Languages
{
    public class DetailsModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DetailsModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public Language Language { get; set; } = default!;
        public List<Book> Books { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public int LanguageId => Language.LanguageId!;

        public async Task<IActionResult> OnGetAsync(int? languageId, string? search)
        {
            if (languageId == null)
            {
                return NotFound();
            }

            Language = await _context.Languages.FirstOrDefaultAsync(m => m.LanguageId == languageId);

            if (Language == null)
            {
                return NotFound();
            }

            IQueryable<Book> booksQuery = _context.Books
                .Include(book => book.Publisher)
                .Include(book => book.BookAuthors)
                .ThenInclude(bookAuthor => bookAuthor.Author);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();

                booksQuery = booksQuery.Where(book => book.LanguageId == Language.LanguageId
                                                      && book.Title.ToLower().Contains(Search));
            }
            else
            {
                booksQuery = booksQuery.Where(book => book.LanguageId == Language.LanguageId);
            }

            Books = await booksQuery
                .OrderBy(book => book.Title)
                .ThenByDescending(book => book.YearPublished)
                .ToListAsync();
            
            return Page();
        }
    }
}