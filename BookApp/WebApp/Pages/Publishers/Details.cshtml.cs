using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Publishers
{
    public class DetailsModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DetailsModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public Publisher Publisher { get; set; } = default!;

        public List<Book> Books { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public int PublisherId => Publisher.PublisherId;

        public async Task<IActionResult> OnGetAsync(int? publisherId, string? search)
        {
            if (publisherId == null)
            {
                return NotFound();
            }

            Publisher = await _context.Publishers.FirstOrDefaultAsync(m => m.PublisherId == publisherId);

            if (Publisher == null)
            {
                return NotFound();
            }

            IQueryable<Book> booksQuery = _context.Books
                .Include(book => book.BookAuthors)
                .ThenInclude(bookAuthor => bookAuthor.Author)
                .Include(book => book.Category);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();

                booksQuery = booksQuery.Where(book => book.PublisherId == Publisher.PublisherId
                                                      && book.Title.ToLower().Contains(Search));
            }
            else
            {
                booksQuery = booksQuery.Where(book => book.PublisherId == Publisher.PublisherId);
            }

            Books = await booksQuery.OrderBy(book => book.Title).ToListAsync();

            return Page();
        }
    }
}