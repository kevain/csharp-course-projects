using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Publishers
{
    public class IndexModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public IndexModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IList<Publisher> Publishers { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            IQueryable<Publisher> publisherQuery = _context.Publishers
                .Include(publisher => publisher.Books);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();
                publisherQuery = publisherQuery
                    .Where(publisher => publisher.PublisherName.ToLower().Contains(Search));
            }

            Publishers = await publisherQuery
                .OrderBy(publisher => publisher.PublisherName)
                .ToListAsync();

            if (Publishers.Count == 1 && search != null)
            {
                return RedirectToPage("./Details", new {publisherId = Publishers.First().PublisherId});
            }

            return Page();
        }
    }
}