using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Publishers
{
    public class DeleteModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Publisher Publisher { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? publisherId)
        {
            if (publisherId == null)
            {
                return NotFound();
            }

            Publisher = await _context.Publishers.FirstOrDefaultAsync(m => m.PublisherId == publisherId);

            if (Publisher == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? publisherId)
        {
            if (publisherId == null)
            {
                return NotFound();
            }

            Publisher = await _context.Publishers.FindAsync(publisherId);

            if (Publisher != null)
            {
                _context.Publishers.Remove(Publisher);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
