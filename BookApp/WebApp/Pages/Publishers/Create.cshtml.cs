using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Publishers
{
    public class CreateModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Publisher Publisher { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || Publisher == null)
            {
                return Page();
            }
            
            var existingPublisher = await _context.Publishers.FirstOrDefaultAsync(publisher =>
                publisher.PublisherName.ToLower().Equals(Publisher.PublisherName.ToLower()));

            if (existingPublisher != null)
            {
                return RedirectToPage("./Details", new {publisherId = existingPublisher.PublisherId});
            }

            if (Publisher == null) return Page();

            _context.Publishers.Add(Publisher);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {publisherId = Publisher.PublisherId});
        }
    }
}