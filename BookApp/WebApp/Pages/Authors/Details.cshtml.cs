using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace WebApp.Pages.Authors
{
    public class DetailsModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DetailsModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public Author Author { get; set; } = default!;
        public List<Book> Books { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public int AuthorId => Author.AuthorId;

        public async Task<IActionResult> OnGetAsync(int? authorId, string? search)
        {
            if (authorId == null)
            {
                return NotFound();
            }

            Author = await _context.Authors.Include(author => author.AuthorBooks)
                .FirstOrDefaultAsync(m => m.AuthorId == authorId);

            if (Author == null)
            {
                return NotFound();
            }

            IQueryable<Book> booksQuery = _context.Books
                .Include(book => book.Publisher)
                .Include(book => book.Category)
                .Include(book => book.BookAuthors);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();

                booksQuery = booksQuery.Where(book =>
                    book.BookAuthors.Any(bookAuthor => bookAuthor.AuthorId == authorId) &&
                    book.Title.ToLower().Contains(Search));
            }
            else
            {
                booksQuery = booksQuery.Where(book =>
                    book.BookAuthors.Any(bookAuthor => bookAuthor.AuthorId == authorId));
            }

            Books = await booksQuery
                .OrderBy(book => book.Title)
                .ThenByDescending(book => book.YearPublished)
                .ToListAsync();

            return Page();
        }
    }
}