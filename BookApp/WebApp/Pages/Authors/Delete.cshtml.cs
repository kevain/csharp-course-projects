using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Authors
{
    public class DeleteModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Author Author { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? authorId)
        {
            if (authorId == null)
            {
                return NotFound();
            }

            Author = await _context.Authors.FirstOrDefaultAsync(m => m.AuthorId == authorId);

            if (Author == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? authorId)
        {
            if (authorId == null)
            {
                return NotFound();
            }

            Author = await _context.Authors.FindAsync(authorId);

            if (Author != null)
            {
                _context.Authors.Remove(Author);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}