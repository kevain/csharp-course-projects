using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Authors
{
    public class IndexModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public IndexModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IList<Author> Authors { get; set; } = default!;

        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            IQueryable<Author> authorQuery = _context.Authors
                .Include(author => author.AuthorBooks);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();
                authorQuery = authorQuery
                    .Where(author => author.FirstName.ToLower().Contains(Search) ||
                                     author.LastName.ToLower().Contains(Search));
            }
            
            Authors = await authorQuery
                .OrderBy(author => author.FirstName)
                .ThenBy(author => author.LastName)
                .ToListAsync();

            if (Authors.Count == 1 && search != null)
            {
                return RedirectToPage("./Details", new {authorId = Authors.First().AuthorId});
            }

            return Page();
        }
    }
}