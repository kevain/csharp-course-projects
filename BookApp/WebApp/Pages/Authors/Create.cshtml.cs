using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Authors
{
    public class CreateModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Author Author { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || Author == null)
            {
                return Page();
            }
            
            _context.Authors.Add(Author);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {authorId = Author.AuthorId});
        }
    }
}