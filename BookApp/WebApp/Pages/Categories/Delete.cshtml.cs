using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Categories
{
    public class DeleteModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DeleteModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public Category Category { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? categoryId)
        {
            if (categoryId == null)
            {
                return NotFound();
            }

            Category = await _context.Categories.FirstOrDefaultAsync(m => m.CategoryId == categoryId);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? categoryId)
        {
            if (categoryId == null)
            {
                return NotFound();
            }

            Category = await _context.Categories.FindAsync(categoryId);

            if (Category != null)
            {
                _context.Categories.Remove(Category);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
