using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Categories
{
    public class IndexModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public IndexModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IList<Category> Categories { get; set; } = default!;
        
        [Required(ErrorMessage = "Please enter search query!")]
        [MinLength(2, ErrorMessage = "Search query must be at least 2 characters!")]
        public string? Search { get; set; }

        public async Task<IActionResult> OnGetAsync(string? search)
        {
            IQueryable<Category> categoriesQuery = _context.Categories
                .Include(category => category.Books);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.Trim().ToLower();
                categoriesQuery = categoriesQuery.Where(category => category.CategoryName.ToLower().Contains(Search));
            }

            Categories = await categoriesQuery
                .OrderBy(category => category.CategoryName)
                .ToListAsync();
            
            if (Categories.Count == 1 && search != null)
            {
                return RedirectToPage("./Details", new {categoryId = Categories[0].CategoryId});
            }
            
            return Page();
        }
    }
}

