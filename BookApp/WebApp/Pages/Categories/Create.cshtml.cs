using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Categories
{
    public class CreateModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public CreateModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Category Category { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || Category == null)
            {
                return Page();
            }

            var existingCategory = await _context.Categories.FirstOrDefaultAsync(category =>
                category.CategoryName.ToLower().Equals(Category.CategoryName.ToLower()));

            if (existingCategory != null)
            {
                return RedirectToPage("./Details", new {categoryId = existingCategory.CategoryId});
            }

            if (Category == null) return Page();

            _context.Categories.Add(Category);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Details", new {categoryId = Category.CategoryId});
        }
    }
}