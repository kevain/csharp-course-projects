using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Categories
{
    public class DetailsModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public DetailsModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public Category Category { get; set; } = default!;
        public List<Book> Books { get; set; } = default!;

        public string? Search { get; set; }

        public int CategoryId => Category.CategoryId;

        public async Task<IActionResult> OnGetAsync(int? categoryId, string? search)
        {
            if (categoryId == null)
            {
                return NotFound();
            }

            Category = await _context.Categories.FirstOrDefaultAsync(m => m.CategoryId == categoryId);

            if (Category == null)
            {
                return NotFound();
            }

            IQueryable<Book> booksQuery = _context.Books
                .Include(book => book.Publisher)
                .Include(book => book.BookAuthors)
                .ThenInclude(bookAuthor => bookAuthor.Author);

            if (!string.IsNullOrWhiteSpace(search))
            {
                Search = search.ToLower();
                
                booksQuery = booksQuery.Where(book =>
                    book.CategoryId == Category.CategoryId && book.Title.ToLower().Contains(Search));
            }
            else
            {
                booksQuery = booksQuery
                    .Where(book => book.CategoryId == Category.CategoryId);
            }

            Books = await booksQuery.ToListAsync();

            return Page();
        }
    }
}