using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.AuthorBooks
{
    public class RemoveBookModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public RemoveBookModel(AppDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty] public AuthorBook AuthorBook { get; set; } = default!;

        public Author Author => AuthorBook.Author!;
        public Book Book => AuthorBook.Book!;

        public async Task<IActionResult> OnGetAsync(int? authorId, int? bookId)
        {
            if (authorId == null || bookId == null)
            {
                return NotFound();
            }

            AuthorBook = await _context.AuthorBooks
                .Include(a => a.Author)
                .Include(a => a.Book)
                .FirstOrDefaultAsync(m => m.AuthorId == authorId && m.BookId == bookId);

            if (AuthorBook == null || Author == null || Book == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? authorId, int? bookId)
        {
            if (authorId == null || bookId == null)
            {
                return NotFound();
            }

            AuthorBook = await _context.AuthorBooks.FirstOrDefaultAsync(authorBook =>
                authorBook.AuthorId == authorId && authorBook.BookId == bookId);

            if (AuthorBook == null) return RedirectToPage("../Authors/Details", new {authorId});

            _context.AuthorBooks.Remove(AuthorBook);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Authors/Details", new {authorId});
        }
    }
}