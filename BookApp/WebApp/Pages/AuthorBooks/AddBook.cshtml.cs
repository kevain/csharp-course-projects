using System.Collections.Generic;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.AuthorBooks
{
    public class AddBookModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public AddBookModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGet(int? authorId)
        {
            if (authorId == null)
            {
                return NotFound();
            }

            Author = await _context.Authors.FirstOrDefaultAsync(author => author.AuthorId == authorId);

            if (Author == null)
            {
                return NotFound();
            }

            AuthorBook = new AuthorBook {AuthorId = (int) authorId};

            Books = await LoadBooks((int) authorId);

            BooksSelectList = new SelectList(Books, nameof(Book.BookId), nameof(Book.Title));


            return Page();
        }

        [BindProperty] public AuthorBook AuthorBook { get; set; } = default!;
        public Author Author { get; set; } = default!;
        private List<Book> Books { get; set; } = default!;
        public SelectList BooksSelectList { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                if (AuthorBook.AuthorId == 0) return NotFound();

                var authorId = AuthorBook.AuthorId;

                Author = await _context.Authors.FirstOrDefaultAsync(author => author.AuthorId == authorId);

                if (Author == null)
                {
                    return NotFound();
                }

                AuthorBook = new AuthorBook {AuthorId = authorId};
                Books = await LoadBooks(authorId);
                BooksSelectList = new SelectList(Books, nameof(Book.BookId), nameof(Book.Title));

                return Page();
            }

            _context.AuthorBooks.Add(AuthorBook);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Authors/Details", new {authorId = AuthorBook.AuthorId});
        }

        private async Task<List<Book>> LoadBooks(int authorId)
        {
            return await _context.Books.FromSqlInterpolated(
                    $"SELECT * FROM Books WHERE bookId NOT IN (SELECT bookId FROM AuthorBooks WHERE AuthorId == {authorId})")
                .ToListAsync();
        }
    }
}