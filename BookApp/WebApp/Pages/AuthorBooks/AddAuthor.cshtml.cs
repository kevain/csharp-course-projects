using System.Collections.Generic;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.AuthorBooks
{
    public class AddAuthorModel : PageModel
    {
        private readonly AppDatabaseContext _context;

        public AddAuthorModel(AppDatabaseContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGet(int? bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }

            Book = await _context.Books.FirstOrDefaultAsync(book => book.BookId == bookId);

            if (Book == null)
            {
                return NotFound();
            }

            AuthorBook = new AuthorBook {BookId = (int) bookId};

            Authors = await LoadAuthors((int) bookId);

            AuthorsSelectList = new SelectList(Authors, nameof(Author.AuthorId), nameof(Author.FirstLastName));


            return Page();
        }

        [BindProperty] public AuthorBook AuthorBook { get; set; } = default!;
        public Book Book { get; set; } = default!;
        private List<Author> Authors { get; set; } = default!;
        public SelectList AuthorsSelectList { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                if (AuthorBook.AuthorId == 0) return NotFound();

                var bookId = AuthorBook.BookId;

                Book = await _context.Books.FirstOrDefaultAsync(book => book.BookId == bookId);

                if (Book == null)
                {
                    return NotFound();
                }

                AuthorBook = new AuthorBook {BookId = bookId};
                Authors = await LoadAuthors(bookId);
                AuthorsSelectList = new SelectList(Authors, nameof(Author.AuthorId), nameof(Author.FirstLastName));

                return Page();
            }

            _context.AuthorBooks.Add(AuthorBook);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Books/Details", new {bookId = AuthorBook.BookId});
        }

        private async Task<List<Author>> LoadAuthors(int bookId)
        {
            return await _context.Authors.FromSqlInterpolated(
                    $"SELECT * FROM Authors WHERE authorId NOT IN (SELECT authorId FROM AuthorBooks WHERE bookId == {bookId})")
                .ToListAsync();
        }
    }
}