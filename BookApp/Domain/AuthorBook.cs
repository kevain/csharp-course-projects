﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class AuthorBook
    {
        [Display(Name = "Author")] public int AuthorId { get; set; }
        public Author? Author { get; set; }

        [Display(Name = "Book")] public int BookId { get; set; }
        public Book? Book { get; set; }
    }
}