﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Publisher
    {
        public int PublisherId { get; set; }

        [Required(ErrorMessage = "Publisher must have a name!")]
        [MinLength(1, ErrorMessage = "Publisher's name must be at least 1 character long!")]
        [MaxLength(120, ErrorMessage = "Publisher's name cannot be longer than 120 characters!")]
        [Display(Name = "Publisher's name")]
        public string PublisherName { get; set; } = default!;

        public ICollection<Book>? Books { get; set; }
    }
}