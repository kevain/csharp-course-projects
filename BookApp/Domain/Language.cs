﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Language
    {
        private string _languageName = default!;
        public int LanguageId { get; set; }
        
        [Display(Name = "Language")]
        [MinLength(2, ErrorMessage = "Language's name must be longer than 2 characters!")]
        [MaxLength(25, ErrorMessage = "Language's name cannot be longer than 25 characters!")]
        [Required(ErrorMessage = "Language must have a name!")]
        public string LanguageName
        {
            get => _languageName;
            set 
            {
                var name = value.ToLower();
                _languageName = $"{name[0].ToString().ToUpper()}{name.Substring(1)}";
            }
        }

        public ICollection<Book>? Books { get; set; }
    }
}