﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Author
    {
        public int AuthorId { get; set; }

        [Required(ErrorMessage = "Author must have a first name!")]
        [MinLength(1, ErrorMessage = "First name must be at least 1 character long!")]
        [MaxLength(50, ErrorMessage = "First name cannot be longer than 50 characters!")]
        [Display(Name = "First name")]
        public string FirstName { get; set; } = default!;

        [Required(ErrorMessage = "Author must have a last name!")]
        [MinLength(1, ErrorMessage = "Last name must be at least 1 character long!")]
        [MaxLength(50, ErrorMessage = "Last name cannot be longer than 50 characters!")]
        [Display(Name = "Last name")]
        public string LastName { get; set; } = default!;

        [Display(Name = "Books")] public ICollection<AuthorBook>? AuthorBooks { get; set; }

        [Display(Name = "Name")] public string FirstLastName => $"{FirstName} {LastName}";
    }
}