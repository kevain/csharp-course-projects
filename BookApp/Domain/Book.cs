﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Book
    {
        public int BookId { get; set; }

        [Required(ErrorMessage = "Book must have a title!")]
        [MinLength(1, ErrorMessage = "Book's title must be at least 1 character long!")]
        [MaxLength(100, ErrorMessage = "Book's title cannot be longer than 100 characters!")]
        public string Title { get; set; } = default!;

        [Display(Name = "Number of pages")]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid number of pages!")]
        public int PageCount { get; set; }

        [Display(Name = "Year published")]
        [Range(0, 3000, ErrorMessage = "Publishing year must be between 0 ... 3000")]
        public int YearPublished { get; set; }

        [Display(Name = "Language")] public int LanguageId { get; set; }
        public Language? Language { get; set; }

        [Display(Name = "Publisher")] public int PublisherId { get; set; }
        public Publisher? Publisher { get; set; }

        [Display(Name = "Category")] public int CategoryId { get; set; }
        public Category? Category { get; set; }

        [Display(Name = "Author(s)")] public ICollection<AuthorBook>? BookAuthors { get; set; }

        [Display(Name = "Comments")] public ICollection<Comment>? Comments { get; set; }
    }
}