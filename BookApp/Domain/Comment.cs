﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Comment
    {
        public int CommentId { get; set; }

        [Required(ErrorMessage = "Please provide content for your comment!")]
        [MinLength(15, ErrorMessage = "Comment must be at least 15 characters long!")]
        [MaxLength(200, ErrorMessage = "Comment must be shorter than 200 characters!")]
        [Display(Name = "Comment")]
        public string CommentText { get; set; } = default!;

        public int BookId { get; set; }
        public Book? Book { get; set; }
    }
}