﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Category
    {
        public int CategoryId { get; set; }
        
        [Required(ErrorMessage = "Category must have a name!")]
        [MinLength(1, ErrorMessage = "Category's name must be at least 1 character long!")]
        [MaxLength(25, ErrorMessage = "Category's name must not be longer than 25 characters!")]
        [Display(Name = "Category")]
        public string CategoryName { get; set; } = default!;

        public ICollection<Book>? Books { get; set; }
    }
}