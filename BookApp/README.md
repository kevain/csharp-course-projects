# BookApp

Bonus homework for extra credit @ C# course. 

A simple web app for listing books and commenting, searching and doing CRUD operations on them. 

## Screenshots

![Index page](./screenshots/screenshot-index.jpg)
![Details page](./screenshots/screenshot-details.jpg)