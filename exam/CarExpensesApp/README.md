# Car Expenses App

Done during the examination @ TalTech IT College C# course. 

Task was to write a simple webapp which would allow to monitor car expenses.

## Screenshots
![Index page](./screenshots/screenshot-index.jpg)
![Details page](./screenshots/screenshot-details.jpg)
