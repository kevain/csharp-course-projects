using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Expenses
{
    public class EditModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public EditModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        [BindProperty] public Expense Expense { get; set; } = default!;

        [BindProperty]
        [Display(Name = "New Expense Category", Prompt = "Enter Name For New Expense Category")]
        public string? NewExpenseCategoryName { get; set; }

        public SelectList ExpenseCategoriesSelectList { get; set; } = default!;
        public SelectList CarsSelectList { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Expense = await _context.Expenses
                .Include(e => e.Car)
                .Include(e => e.ExpenseCategory).FirstOrDefaultAsync(m => m.ExpenseId == id);

            if (Expense == null)
            {
                return NotFound();
            }

            ViewData["CarId"] = new SelectList(_context.Cars, "CarId", "CarMaker");
            ViewData["ExpenseCategoryId"] =
                new SelectList(_context.ExpenseCategories, "ExpenseCategoryId", "CategoryName");

            ExpenseCategoriesSelectList = new SelectList(
                _context.ExpenseCategories,
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.CategoryName));

            CarsSelectList =
                new SelectList(
                    _context.Cars
                        .OrderBy(c => c.CarMaker)
                        .ThenBy(c => c.CarModel)
                        .ThenBy(c => c.CarMadeYear),
                    nameof(Car.CarId),
                    nameof(Car.MakerModelYear));

            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Expense).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExpenseExists(Expense.ExpenseId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ExpenseExists(long id)
        {
            return _context.Expenses.Any(e => e.ExpenseId == id);
        }
    }
}