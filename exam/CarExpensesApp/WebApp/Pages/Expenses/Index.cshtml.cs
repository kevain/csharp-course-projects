using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Expenses
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Expense> Expenses { get; set; } = default!;

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string Query { get; set; } = default!;

        public async Task OnGetAsync(string? query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                Expenses = await _context.Expenses
                    .Include(e => e.Car)
                    .Include(e => e.ExpenseCategory)
                    .OrderByDescending(e => e.ExpenseDateTime)
                    .ToListAsync();
            }
            else
            {
                query = query.Trim().ToLower();

                Expenses = await _context.Expenses
                    .Include(e => e.Car)
                    .Include(e => e.ExpenseCategory)
                    .Where(e => e.ExpenseDescription.ToLower().Contains(query) ||
                                e.ExpenseCategory!.CategoryName.ToLower().Contains(query) ||
                                e.Car!.CarMaker.ToLower().Contains(query) ||
                                e.Car.CarModel.ToLower().Contains(query))
                    .OrderByDescending(e => e.ExpenseDateTime)
                    .ToListAsync();
            }
        }
    }
}