using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;

namespace WebApp.Pages_Expenses
{
    public class CreateModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public CreateModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet(int? carId)
        {
            ExpenseCategorySelectList = new SelectList(_context.ExpenseCategories,
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.CategoryName));

            CarSelectList = new SelectList(_context.Cars,
                nameof(Car.CarId),
                nameof(Car.MakerModelYear),
                carId);
            return Page();
        }

        [BindProperty] public Expense Expense { get; set; } = default!;

        [BindProperty]
        [Display(Name = "New Expense Category", Prompt = "Enter Name For New Expense Category")]
        public string? NewExpenseCategoryName { get; set; }

        public SelectList ExpenseCategorySelectList { get; set; } = default!;
        public SelectList CarSelectList { get; set; } = default!;

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                CreateSelectLists();
                return Page();
            }

            if (Expense.ExpenseCategoryId == -1 && string.IsNullOrWhiteSpace(NewExpenseCategoryName))
            {
                ModelState.AddModelError(nameof(NewExpenseCategoryName), "Name for new expense category is required!");
                CreateSelectLists();
                return Page();
            }

            if (Expense.ExpenseCategoryId == -1 && !string.IsNullOrWhiteSpace(NewExpenseCategoryName))
            {
                var newCategory = new ExpenseCategory() {CategoryName = NewExpenseCategoryName.Trim()};
                _context.ExpenseCategories.Add(newCategory);
                Expense.ExpenseCategory = newCategory;
            }

            _context.Expenses.Add(Expense);
            await _context.SaveChangesAsync();

            return RedirectToPage("/Cars/Details", new {id = Expense.CarId});
        }

        private void CreateSelectLists()
        {
            ExpenseCategorySelectList = new SelectList(_context.ExpenseCategories,
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.CategoryName));

            CarSelectList = new SelectList(_context.Cars,
                nameof(Car.CarId),
                nameof(Car.MakerModelYear));
        }
    }
}