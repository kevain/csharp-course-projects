using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Cars
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<Car> Cars { get; set; } = default!;

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string Query { get; set; } = default!;

        public async Task OnGetAsync(string? query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                Cars = await _context.Cars
                    .OrderBy(c => c.CarMaker)
                    .ThenBy(c => c.CarModel)
                    .ThenBy(c => c.CarMadeYear)
                    .ToListAsync();
            }
            else
            {
                query = query.ToLower().Trim();
                int.TryParse(query, out var queryAsInt);

                Cars = await _context.Cars
                    .Where(
                        c => c.CarMaker.ToLower().Contains(query) ||
                             c.CarModel.ToLower().Contains(query) ||
                             c.CarMadeYear.Equals(queryAsInt))
                    .ToListAsync();
            }
        }
    }
}