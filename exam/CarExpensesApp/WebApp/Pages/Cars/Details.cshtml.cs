using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_Cars
{
    public class DetailsModel : PageModel
    {
        private readonly DAL.AppDbContext _context;
        private string _query = default!;

        public DetailsModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public Car Car { get; set; } = default!;

        public long Id => Car.CarId;

        public List<Expense> Expenses => Car.Expenses.OrderByDescending(e => e.ExpenseDateTime).ToList();

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string Query
        {
            get => _query;
            set => _query = value.ToLower().Trim();
        }

        public async Task<IActionResult> OnGetAsync(long? id, string? query)
        {
            if (id == null)
            {
                return NotFound();
            }

            Car = await _context.Cars.FirstOrDefaultAsync(c => c.CarId == id);

            if (Car == null)
            {
                return NotFound();
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                Query = query;

                Car.Expenses = await _context.Expenses
                    .Include(expense => expense.ExpenseCategory)
                    .Where(e => (e.ExpenseDescription.ToLower().Contains(Query) ||
                                 e.ExpenseCategory!.CategoryName.ToLower().Contains(Query)) && e.CarId == Car.CarId)
                    .ToListAsync();
            }
            else
            {
                Car.Expenses = await _context.Expenses
                    .Include(e => e.ExpenseCategory)
                    .Where(e => e.CarId == Car.CarId)
                    .ToListAsync();
            }

            return Page();
        }
    }
}