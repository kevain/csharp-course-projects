﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.DTO;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.AppDbContext _context;

        public IndexModel(ILogger<IndexModel> logger, AppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        // public List<CarExpenseTotalDTO> CarExpenseTotalDtos { get; set; } = default!;

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string? Query { get; set; }

        [Display(Name = "Date From")]
        [DataType(DataType.Date)]
        [BindProperty(SupportsGet = true)]
        public DateTime? DateFrom { get; set; }

        [Display(Name = "Date To")]
        [DataType(DataType.Date)]
        [BindProperty(SupportsGet = true)]
        public DateTime? DateTo { get; set; }

        public int CategoryId { get; set; }

        public List<Car> Cars { get; set; } = default!;

        public SelectList ExpenseCategorySelectList { get; set; } = default!;

        public async void OnGet(string? query, int? categoryId)
        {
            ExpenseCategorySelectList = new SelectList(
                _context.ExpenseCategories.OrderBy(e => e.CategoryName),
                nameof(ExpenseCategory.ExpenseCategoryId),
                nameof(ExpenseCategory.CategoryName));

            // TODO: HOW TO EXECUTE CUSTOM SQL AND LOAD INTO DTO?
            // CarExpenseTotalDtos =
            //     await _context.Cars.FromSqlRaw(
            //         "SELECT c.*, SUM(e.ExpenseSum) FROM Cars c, Expenses e WHERE c.CarId == e.CarId").ToListAsync();

            IQueryable<Car> dbQuery = _context.Cars.Include(c => c.Expenses);

            // TODO: HOW TO GET ONLY SELECTED CATEGORIES?
            if (categoryId != null && categoryId != -1)
            {
                dbQuery = dbQuery.Where(c => c.Expenses.Any(e => e.ExpenseCategoryId == categoryId));
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                query = query.ToLower().Trim();

                dbQuery = dbQuery.Where(c =>
                    c.CarMaker.ToLower().Contains(query) || c.CarModel.ToLower().Contains(query));
            }

            Cars = await dbQuery.ToListAsync();

            if (categoryId != null && categoryId != -1)
            {
                foreach (var car in Cars)
                {
                    car.Expenses = car.Expenses.Where(e => e.ExpenseCategoryId == categoryId).ToList();
                }
            }

            if (DateFrom != null)
            {
                foreach (var car in Cars)
                {
                    car.Expenses = car.Expenses.Where(e => e.ExpenseDateTime >= DateFrom).ToList();
                }
            }

            if (DateTo != null)
            {
                foreach (var car in Cars)
                {
                    car.Expenses = car.Expenses.Where(e => e.ExpenseDateTime <= DateTo).ToList();
                }
            }
        }

        public int GetTotalExpenses()
        {
            var total = 0;

            foreach (var car in Cars)
            {
                total += car.Expenses.Count;
            }

            return total;
        }

        public int GetTotalExpenseSum()
        {
            var total = 0;

            foreach (var car in Cars)
            {
                foreach (var expense in car.Expenses)
                {
                    total += expense.ExpenseSum;
                }
            }

            return total;
        }
    }
}