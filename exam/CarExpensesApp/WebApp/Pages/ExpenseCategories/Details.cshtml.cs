using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_ExpenseCategories
{
    public class DetailsModel : PageModel
    {
        private readonly DAL.AppDbContext _context;
        private string _query = default!;

        public DetailsModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public ExpenseCategory ExpenseCategory { get; set; } = default!;
        public List<Expense> Expenses => new List<Expense>(ExpenseCategory!.Expenses!);
        public long Id => ExpenseCategory!.ExpenseCategoryId;

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string Query
        {
            get => _query;
            set => _query = value.ToLower().Trim();
        }


        public async Task<IActionResult> OnGetAsync(long? id, string? query)
        {
            if (id == null)
            {
                return NotFound();
            }

            ExpenseCategory = await _context.ExpenseCategories
                .FirstOrDefaultAsync(ec => ec.ExpenseCategoryId == id);

            if (ExpenseCategory == null)
            {
                return NotFound();
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                Query = query;
                ExpenseCategory.Expenses = await _context.Expenses
                    .Include(e => e.Car)
                    .Where(e => (e.ExpenseDescription.ToLower().Contains(Query) ||
                                 e.Car!.CarMaker.ToLower().Contains(Query) ||
                                 e.Car!.CarModel.ToLower().Contains(Query)) &&
                                e.ExpenseCategoryId == ExpenseCategory.ExpenseCategoryId)
                    .OrderByDescending(e => e.ExpenseDateTime)
                    .ToListAsync();
            }
            else
            {
                ExpenseCategory.Expenses = await _context.Expenses
                    .Include(e => e.Car)
                    .Where(e => e.ExpenseCategoryId == ExpenseCategory.ExpenseCategoryId)
                    .OrderByDescending(e => e.ExpenseDateTime)
                    .ToListAsync();
            }

            return Page();
        }
    }
}