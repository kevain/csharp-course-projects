using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Pages_ExpenseCategories
{
    public class IndexModel : PageModel
    {
        private readonly DAL.AppDbContext _context;
        private string _query = default!;

        public IndexModel(DAL.AppDbContext context)
        {
            _context = context;
        }

        public IList<ExpenseCategory> ExpenseCategory { get; set; } = default!;

        [Display(Name = "Search")]
        [MinLength(2, ErrorMessage = "{0} should be at least {1} characters!")]
        public string Query
        {
            get => _query;
            set => _query = value.ToLower().Trim();
        }

        public async Task OnGetAsync(string? query)
        {
            IQueryable<ExpenseCategory> dbQuery = _context.ExpenseCategories;

            if (!string.IsNullOrWhiteSpace(query))
            {
                Query = query;

                dbQuery = dbQuery.Where(c => c.CategoryName.ToLower().Contains(Query));
            }

            ExpenseCategory = await dbQuery
                .OrderBy(c => c.CategoryName.ToLower())
                .ToListAsync();
        }
    }
}