﻿using Domain;

namespace WebApp.DTO
{
    public class CarExpenseTotalDTO
    {
        public Car Car { get; set; } = default!;
        public int ExpenseTotal { get; set; }
    }
}