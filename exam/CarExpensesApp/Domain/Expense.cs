﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Expense
    {
        public long ExpenseId { get; set; }

        [Required(ErrorMessage = "Please provide a {0}!")]
        [MaxLength(250, ErrorMessage = "{0} must be shorter than {1} characters!")]
        [MinLength(3, ErrorMessage = "{0} must be longer than {1} characters!")]
        [Display(Name = "Description", Prompt = "Enter Description For Expense...")]
        public string ExpenseDescription { get; set; } = default!;

        [Required(ErrorMessage = "Please provide a {0} for the expense!")]
        [Range(0, int.MaxValue, ErrorMessage = "{0} must be between {1}...{2}")]
        [Display(Name = "Sum", Prompt = "Enter Expense's Sum...")]
        public int ExpenseSum { get; set; }

        [Required(ErrorMessage = "Please provide a {0} for the expense!")]
        [Display(Name = "Date", Prompt = "Enter Expense's Date...")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpenseDateTime { get; set; }

        [Display(Name = "Category")] public ExpenseCategory? ExpenseCategory { get; set; }
        [Display(Name = "Category")] public long ExpenseCategoryId { get; set; }

        [Display(Name = "Car")] public Car? Car { get; set; }
        [Display(Name = "Car")] public long CarId { get; set; }
    }
}