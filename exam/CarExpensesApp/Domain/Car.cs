﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Domain
{
    public class Car
    {
        public long CarId { get; set; }

        [Required(ErrorMessage = "Please provide a {0} for the car!")]
        [Display(Name = "Maker", Prompt = "Enter Car Maker...")]
        [MinLength(2, ErrorMessage = "{0} must be at least {1} characters!")]
        [MaxLength(20, ErrorMessage = "{0} must be shorter than {1} characters!")]
        public string CarMaker { get; set; } = default!;

        [Required(ErrorMessage = "Please provide a {0} for the car!")]
        [Display(Name = "Model", Prompt = "Enter Car Model...")]
        [MinLength(2, ErrorMessage = "{0} must be at least {1} characters!")]
        [MaxLength(30, ErrorMessage = "{0} must be shorter than {1} characters!")]
        public string CarModel { get; set; } = default!;

        [Required(ErrorMessage = "Please provide a manufacturing year for the car!")]
        [Display(Name = "Year Manufactured", Prompt = "Enter Manufacturing Year...")]
        [Range(1885, 2020, ErrorMessage = "{0} must be between {1}...{2}")]
        public int CarMadeYear { get; set; } = DateTime.Now.Year;

        public ICollection<Expense>? Expenses { get; set; }

        [Display(Name = "Car")] public string MakerModelYear => $"{CarMaker} {CarModel} ({CarMadeYear})";

        [Display(Name = "Total Expenses")] public int TotalExpenseSum => Expenses?.Sum(expense => expense.ExpenseSum) ?? 0;
    }
}