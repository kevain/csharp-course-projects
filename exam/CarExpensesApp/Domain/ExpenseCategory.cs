﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ExpenseCategory
    {
        public long ExpenseCategoryId { get; set; }

        [Required(ErrorMessage = "Please provide a {0}!")]
        [MaxLength(15, ErrorMessage = "{0} must be shorter than {1} characters!")]
        [MinLength(3, ErrorMessage = "{0} must be longer than {1} characters")]
        [Display(Name = "Category Name", Prompt = "Enter Name For Category...")]
        public string CategoryName { get; set; } = default!;

        public ICollection<Expense>? Expenses { get; set; }
    }
}