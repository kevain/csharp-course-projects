﻿using System;
using System.IO;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public static readonly string DbPath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "CarExpensesAppDb.Db");

        public DbSet<Car> Cars { get; set; } = default!;
        public DbSet<Expense> Expenses { get; set; } = default!;
        public DbSet<ExpenseCategory> ExpenseCategories { get; set; } = default!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=" + DbPath);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<ExpenseCategory>()
                .HasIndex(c => c.CategoryName)
                .IsUnique();
        }
    }
}